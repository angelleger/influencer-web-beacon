'use strict';

const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const OAuth2Client = google.auth.OAuth2;
const http = require('http');
const nconf = require('nconf');
const tracker = require('pixel-tracker');
var MongoClient = require('mongodb').MongoClient;
var CronJob = require('cron').CronJob;

const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
const TOKEN_PATH = 'credentials.json';


// Read in keys and secrets. Using nconf use can set secrets via
// environment variables, command-line arguments, or a keys.json file.
nconf.argv().env().file('keys.json');

const user = nconf.get('mongoUser');
const pass = nconf.get('mongoPass');
const host = nconf.get('mongoHost');
const port = nconf.get('mongoPort');
const database = nconf.get('mongoDatabase');

let uri = `mongodb://${user}:${pass}@${host}:${port}`;
if (nconf.get('mongoDatabase')) {
    uri = `${uri}/${nconf.get('mongoDatabase')}`;
}

tracker
    .use(function(error, result) {


        result.visit_date = new Date();
        saveEntry(result);

    })
    .configure({
        disable_cookies: true
    });

//00 59 23 * * 0-6 running cron once a day
var job = new CronJob('15 * * * * 0-6', function() {
    fs.readFile('client_secret.json', (err, content) => {
        if (err) return console.log('Error loading client secret file:', err);
        // Authorize a client with credentials, then call the Google Sheets API.
        authorize(JSON.parse(content), listUrls); 
        authorize(JSON.parse(content), setAllTrafficInSheet);
    });
        console.log("Updating Spreedsheet");
    }, function() {

    },
    true, /* Start the job right now */
    'America/Los_Angeles',
);



/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    const {
        client_secret,
        client_id,
        redirect_uris
    } = credentials.installed;
    const oAuth2Client = new OAuth2Client(client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return getNewToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return callback(err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}

/**
 * Someone call the Beacon so let's save the entry on the DB
 * @param {Object} callback The callback to call.
 */
function saveEntry(entry) {
    MongoClient.connect(uri, function(err, db) {
        if (err) throw err;
        var dbo = db.db(database);
        dbo.collection("entries").insertOne(entry, function(err, res) {
            if (err) throw err;
            console.log("Someone call the Beacon");
            db.close();
        });
    });

}

/**
 * let's get the total traffic count by URL
 * @param {String} url The published URL.
 * @param {Number} i The published URL number in the array spreedsheet
 * @param {function} callback The callback to call.
 */
function getUrlTrafficCount(url, i, callback) {

    MongoClient.connect(uri, function(err, db) {
        if (err) throw err;
        var dbo = db.db(database);
        dbo.collection('entries').count({
            referer: url
        }, function(err, count) {

            if (err) throw err;

            db.close();
            // Changing the array position to the real published URL number in the spreedsheet
            var num = i + 2;

            callback(count, num, url);
        });

    });

}


/**
 * Let's save the "total traffic from web beacon" in the spreedsheet
 * @param {Object} auth The authorization client credentials.
 * @param {Object} rows The callback to call with the authorized client.
 */
function setTrafficCountSheet(auth, rows) {

    for (var i = 0, len = rows.length; i < len; i++) {

        getUrlTrafficCount(rows[i], i, function(response, num, url) {
            var counts = [];
            counts.push(response);

            // let's ensure that everything it's executed
            const sheets = google.sheets({
                version: 'v4',
                auth
            });

            sheets.spreadsheets.values.update({
                spreadsheetId: '1PsRwvZRLf_ZsxiLeMpxcuxlX3WC4lIJjtTvruEG5tDo',
                range: 'Rank!O' + num,
                valueInputOption: 'USER_ENTERED',

                resource: {
                    values: [
                        counts
                    ],
                    majorDimension: 'COLUMNS',
                },

            }, (err, {
                data
            }) => {
                if (err) return console.log('The API returned an error: ' + err);

            });

        });

    }

}

/**
 * Let's save the "total traffic from web beacon" in the spreedsheet
 * @param {Object} auth The authorization client credentials.
 * @param {Object} rows The callback to call with the authorized client.
 */
function setAllTrafficInSheet(auth) {
    var traffic;

    // Getting all Traffic
    MongoClient.connect(uri, function(err, db) {
        if (err) throw err;
        var dbo = db.db(database);

        dbo.collection("entries").find({}, {
            projection: {
                _id: 0,
                cache: 0,
                language: 0,
                cookies: 0,
                params: 0,
                decay: 0,
                useragent: 0,
                geo: 0
            }
        }).toArray(function(err, result) {
            if (err) throw err;

            traffic = result;
            db.close();


            for (var i = 0, len = traffic.length; i < len; i++) {
                var rowNumber = i + 2;

                let dataTraffic = Object
                    .keys(traffic[i])
                    .map(key => traffic[i][key]);


                // let's ensure that everything it's executed
                const sheets = google.sheets({
                    version: 'v4',
                    auth
                });


                sheets.spreadsheets.values.update({
                    spreadsheetId: '1PsRwvZRLf_ZsxiLeMpxcuxlX3WC4lIJjtTvruEG5tDo',
                    range: 'Web Beacon Track!A' + rowNumber,
                    valueInputOption: 'USER_ENTERED',

                    resource: {
                        values: [dataTraffic],
                        majorDimension: 'ROWS',
                    },

                }, (err, {
                    data
                }) => {
                    if (err) return console.log('The API returned an error: ' + err);

                });

            }

        });

    });



}


/**
 * Getting the "published URL" from the spreddsheet in the Column N:
 * @see https://docs.google.com/spreadsheets/d/1PsRwvZRLf_ZsxiLeMpxcuxlX3WC4lIJjtTvruEG5tDo/edit
 * @param {OAuth2Client} auth The authenticated Google OAuth client.
 */
function listUrls(auth) {

    const sheets = google.sheets({
        version: 'v4',
        auth
    });
    sheets.spreadsheets.values.get({
        spreadsheetId: '1PsRwvZRLf_ZsxiLeMpxcuxlX3WC4lIJjtTvruEG5tDo',
        range: 'Rank!N2:N',
        majorDimension: "COLUMNS",
    }, (err, {
        data
    }) => {
        if (err) return console.log('The API returned an error: ' + err);
        const rows = data.values[0];

        if (rows.length) {

            setTrafficCountSheet(auth, rows);

        } else {
            console.log('No data found.');
        }
    });
}


// Starting the Server
require('http').createServer(tracker.middleware).listen(8080)